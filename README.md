# Routes

## Portugal

```mermaid
flowchart TD
    naz(Nazaré) -->|1h| Ericeira -->|30m| Lisbon ---->|1h| Comporta ---->|1h 30| vndm(Vila Nova de Milfontes) --->|45m| Odeceixe -->|30m| Bordeira -->|30m| Sagres
    Sagres -->|30m| Lagos -->|2h 45| Lisbon
    Lisbon --> Ericeira ----> naz

    style Lisbon fill:green,color:white
    style Lagos fill:red,color:white
    style naz fill:orange,color:white
```
